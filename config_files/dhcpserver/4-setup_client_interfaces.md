### Client interfaces - how to connect to dhcp network (no internet yet)
##### Host-Only interfaces
Setup Vmware Lanb adapters as Host Only (no specific config). If it was configured as NAT, it may not have internet anymore until fully configured.
##### /etc/network/interfaces
`auto ens33
allow-hotplug ens33
iface ens33 inet dhcp`

##### Adding interfaces may add default gateway
Better to add interfaces 1 by 1 and try 1 at a time. Disable all unecessary stuff.
Eventually:
`systemctl restart networking`
will restart up/down states. This should be done manually in case of issue. EG, if this cmd takes ages there is obviously an issue (moslty DHCP OFFER catching timouet)

##### Network-manager
Is not used, otherwise interfaces would need to be removed from `/etc/network/interfaces`

##### catch DHCPOFFER
* ifdown ens33
* ifup ens33
Check if 'ip a' return DHCP range ip
if not, retry
check ping gateway/router
##### loosy W/A
modify DHCP client in dhclient.conf (/etc/dhcp/)
uncomment:
alias {
    interface "ens33";
}
Eventually try forcing options here.
### IP FORWARD
##### IP forwarding, did not provide internet to client by itself.

#### Get status
* sysctl net.ipv4.ip_forward
* cat /proc/sys/net/ipv4/ip_forward
#### cmds
* net.ipv4.ip_forward = 1" >> /etc/sysctl.conf
* echo 1 >/proc/sys/net/ipv4/ip_forward

#### Persistency
* modify sysctl.conf with net.ipv4.ip_forward = 1" >> /etc/sysctl.conf
* sysctl -p /etc/sysctl.conf

#### untested cmds
* /etc/init.d/procps.sh restart

#### distro specific (untested)
/etc/network/options:
ip_forward=no

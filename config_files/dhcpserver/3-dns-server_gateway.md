### DNS SERVER on the Gateway/Router
Usual dns options are located on `/etc/dhcp/dhcp.conf`

`option domain-name servers 192.168.1.254, 8.8.4.4`

primary is often the gateway itself, while secondary is google secondary/primary (backup).
Obvisouly here, secondary may hardly be used as dns will be provided by gateway configuration and will not likely fail. Indeed in this setup, if gateway fall then  the whole network fall (including dns).

Tweak dns only once setup is fully functionnal and backup done.
##### Some dns server can be setup unsing BIND9
It is not the purpose of this setup